﻿using System;

public class Class1
{
    public partial class Class1 : ContentPage
    {
        public Class1()
        {
            InitializeComponent();
        }

        async void OnBackClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}

