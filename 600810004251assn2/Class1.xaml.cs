﻿using System;

namespace _600810004251assn2
{
    public partial class Class1 : ContentPage
    {
        public Class1()
        {
            InitializeComponent();
        }

        async void OnBackClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}
